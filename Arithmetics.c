#include"Arithmetics.h"

///Note: new variables are created and remembered
//Sum
void* sumInt(void* a1, void* a2) {
    int *ia1 = (int *) a1;
    int *ia2 = (int *) a2;
    int *res = malloc(sizeof(int));
    *res = *ia1 + *ia2;
    return (void *) res;
}
void* sumDouble(void* a1, void* a2) {
    double *ia1 = (double *) a1;
    double *ia2 = (double *) a2;
    double *res = malloc(sizeof(double));
    *res = *ia1 + *ia2;
    return (void *) res;
}
void* sumComplex(void* a1, void* a2) {
    complex *ia1 = (complex *) a1;
    complex *ia2 = (complex *) a2;
    complex *res = malloc(sizeof(complex));
    res->Re = ia1->Re + ia2->Re;
    res->Im = ia1->Im + ia2->Im;
    return (void *) res;
}
//Subtraction
void* minusInt(void *a1, void *a2) {
    int *ia1 = (int *) a1;
    int *ia2 = (int *) a2;
    int *res = malloc(sizeof(int));
    *res = *ia1 - *ia2;
    return (void *) res;
}
void* minusDouble(void *a1, void *a2) {
    double *ia1 = (double *) a1;
    double *ia2 = (double *) a2;
    double *res = malloc(sizeof(double));
    *res = *ia1 - *ia2;
    return (void *) res;
}
void* minusComplex(void *a1, void *a2) {
    complex *ia1 = (complex *) a1;
    complex *ia2 = (complex *) a2;
    complex *res = malloc(sizeof(complex));
    res->Re = ia1->Re - ia2->Re;
    res->Im = ia1->Im - ia2->Im;
    return (void *) res;
}
//Multiplication
void* multInt(void *a1, void *a2) {
    int *ia1 = (int *) a1;
    int *ia2 = (int *) a2;
    int *res = malloc(sizeof(int));
    *res = *ia1 * *ia2;
    return (void *) res;
}
void* multDouble(void *a1, void *a2) {
    double *ia1 = (double *) a1;
    double *ia2 = (double *) a2;
    double *res = malloc(sizeof(double));
    *res = *ia1 * *ia2;
    return (void *) res;
}
void* multComplex(void *a1, void *a2) {
    complex *ia1 = (complex *)a1;
    complex *ia2 = (complex *)a2;
    complex *res = malloc(sizeof(complex));
    res->Re = ia1->Re * ia2->Re - ia1->Im * ia2->Im;
    res->Im = ia1->Re * ia2->Im + ia1->Im * ia2->Re;
    return (void*) res;
}



int len(char *a)
{
    int i = 0;
    while (a[i] != '\0')
        i++;
    return i;
}

void *sumString(void *a1, void *a2) {
    char *b1 = (char*) a1, *b2 = (char*) a2;
    int l1 = len(b1), l2 = len(b2), l = l1 + l2;
    char *res = malloc(l + 1);
    int i = 0;
    for(i; i < l1; i++)
        res[i] = b1[i];
    for(i; i < l; i++)
        res[i] = b2[i - l1];
    res[l] = '\0';
    return (void*) res;
}

///End of Arithmetics.h