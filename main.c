#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Console_Menu_Functions.h"
#include "vector.h"

// Comments - \, /; 0 -- *; 1 -- -; 2 -- .; errors -- <.

int main() {

    printf("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\nWelcome to LABA1!!\nI am Maxim Kudrin from b21-514\nThis program can work with 3D vectors. The program can calculate:\nSum of 2 vectors\nDot product of 2 vectors\nCross product of 2 vectors\n///////////////\n\nTo start, please, type any symbol\n");
    getchar();
    printf("\n\n");
    vector *vectArray = 0;
    int size_v = 0;
    while (1)
    {
        printf("***************\nWhat do we do?\n0 -- Quit\n1 -- Create/delete a vector\n2 -- Look at vectors\n3 -- Make a calculation\n***************\n");
        int choice;
        if (getInt(&choice))
        {
            if (choice == 0)
            {
                int i = 0;
                for(i; i < size_v; i++)
                    forget(&(vectArray[i]));
                if (vectArray != 0)
                    free(vectArray);
                break;
            }
            if (choice == 1)
            {
                push_delVectArr(&vectArray, &size_v);
            }
            else if (choice == 2)
            {
                prtVectors(vectArray, size_v);
            }
            else if (choice == 3)
            {
                calc(&vectArray, size_v);
            }
            else
                printf("There is no such option: %d. Please, try again...\n", choice);
        }

    }
    return 0;
}
