#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "vector.h"
#include "Arithmetics.h"

#ifndef CPROJECT_Console_Menu_Functions_H
#define CPROJECT_Console_Menu_Functions_H

#endif //CPROJECT_Console_Menu_Functions_H

void err1();
void err6();

//1 == success, 0 == fail
int getInt(int *a);
int getDouble(double *a);
int getComplex(complex *a);
int getCharPointers(void **a);

int push_delVectArr(vector **vectArr, int *n);
int prtVectors(const vector *vectArr, const int n);

int calc(vector **vectArr, const int n);