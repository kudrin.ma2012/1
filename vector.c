#include "vector.h"

vector* create(void* (*sum)(void*, void*), void* (*mult)(void*, void*), void* (*minus)(void*, void*), int size)
{
    vector* res = malloc(sizeof(vector));
    res->sum = sum;
    res->minus = minus;
    res->mult = mult;
    res->x = 0;
    res->y = 0;
    res->z = 0;
    res->size = size;
    return res;
}

void vectSum(vector* v1, vector* v2)
{
    if (v1->size != v2->size && !(v1->mult == 0 && v2->mult == 0))
    {
        err1();
        return;
    }
    void *x1 = v1->sum(v1->x, v2->x);
    free(v1->x);
    v1->x = x1;
    void *y1 = v1->sum(v1->y, v2->y);
    free(v1->y);
    v1->y = y1;
    void *z1 = v1->sum(v1->z, v2->z);
    free(v1->z);
    v1->z = z1;
}

void *dotProduct(vector* v1, vector *v2)
{
    if (!v1->mult || !v2->mult)
    {
        err6();
        return 0;
    }
    if (v1->size != v2->size)
    {
        err1();
        return 0;
    }
    void *res = v1->mult(v1->x, v2->x);

    void *m1 = v1->mult(v1->y, v2->y), *s1 = v1->sum(res, m1);
    free(m1);
    free(res);
    res = s1;

    m1 = v1->mult(v1->z, v2->z);
    s1 = v1->sum(res, m1);
    free(m1);
    free(res);
    res = s1;
    return res;
}

void crossProduct(vector *v1, vector *v2) {
    if (!v1->mult || !v2->mult)
    {
        err6();
        return;
    }
    if (v1->size != v2->size )
    {
        err1();
        return;
    }
    vector v1c = *v1, v2c = *v2;
    v1c.x = malloc(v1->size);
    memcpy(v1c.x, v1->x, v1->size);
    v1c.y = malloc(v1->size);
    memcpy(v1c.y, v1->y, v1->size);
    v1c.z = malloc(v1->size);
    memcpy(v1c.z, v1->z, v1->size);
    v2c.x = malloc(v1->size);
    memcpy(v2c.x, v2->x, v1->size);
    v2c.y = malloc(v1->size);
    memcpy(v2c.y, v2->y, v1->size);
    v2c.z = malloc(v1->size);
    memcpy(v2c.z, v2->z, v1->size);
    forget(v1);

    void *m1 = v1->mult(v1c.y, v2c.z), *m2 = v1->mult(v1c.z, v2c.y), *s1 = v1->minus(m1, m2);
    v1->x = s1;
    free(m1); free(m2);

    m1 = v1->mult(v1c.z, v2c.x); m2 = v1->mult(v1c.x, v2c.z); s1 = v1->minus(m1, m2);
    v1->y = s1;
    free(m1); free(m2);

    m1 = v1->mult(v1c.x, v2c.y); m2 = v1->mult(v1c.y, v2c.x); s1 = v1->minus(m1, m2);
    v1->z = s1;
    free(m1); free(m2);


    forget(&v1c);
    forget(&v2c);
}

void forget(vector *a) {
    if (!a)
        return;
    if ((a->x) != 0)
        free(a->x);
    if ((a->y) != 0)
        free(a->y);
    if ((a->z) != 0)
        free(a->z);
}
