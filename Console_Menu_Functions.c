#include "Console_Menu_Functions.h"

///Errors
void err1() {
    printf("<<<<<<<<<<<<<<<<<<<<<<<<\nError #1: different types of operands\n<<<<<<<<<<<<<<<<<<<<<<<<\n");
}

void err2() {
    printf("<<<<<<<<<<<<<<<<<<<<<<<<\nError #2: too many symbols, try again...\n<<<<<<<<<<<<<<<<<<<<<<<<\n");
}

void err3(){
    printf("<<<<<<<<<<<<<<<<<<<<<<<<\nError #3: not an integer, try again...\n<<<<<<<<<<<<<<<<<<<<<<<<\n");
}

void err4(){
    printf("<<<<<<<<<<<<<<<<<<<<<<<<\nError #4: not a double, try again...\n<<<<<<<<<<<<<<<<<<<<<<<<\n");
}

void err5(){
    printf("<<<<<<<<<<<<<<<<<<<<<<<\nError #5: invalid value, try again...\n<<<<<<<<<<<<<<<<<<<<<<<\n");
}

void err6(){
    printf("<<<<<<<<<<<<<<<<<<<<<<<\nWould someone tell me, how to calculate that with strings?\n<<<<<<<<<<<<<<<<<<<<<<<\n");
}


int strtoint(char *c, int *a) {
    *a = 0;
    int negative = 0;
    if (c[0] == '-')
    {
        negative = 1;
        int i = 0;
        for (i; i < strlen(c); i++)
            c[i] = c[i + 1];
    }
    int len = strlen(c);
    if (len > 8) {
        err2();
        return 0;
    }
    int i;
    for (i = 0; i < strlen(c); i++) {
        if (c[i] > '9' || c[i] < '0') {
            err3();
            return 0;
        }
        *a = (*a) * 10 + (c[i] - '0');
    }
    if (negative)
        *a = *a * -1;
    return 1;
}

int strtodouble(char *c, double *a) {
    *a = 0;
    int negative = 0;
    if (c[0] == '-')
    {
        negative = 1;
        int i = 0;
        for (i; i < strlen(c); i++)
            c[i] = c[i + 1];
    }
    int len = strlen(c);
    if (len > 15) {
        err2();
        return 0;
    }
    int i;
    double divideBy = 0;
    for (i = 0; i < strlen(c); i++) {
        if (c[i] == '.')
        {
            if (divideBy != 0)
            {
                err4();
                return 0;
            }
            divideBy = 1;
            continue;
        }
        if (c[i] > '9' || c[i] < '0') {
            err4();
            return 0;
        }
        *a = (*a) * 10 + (c[i] - '0');
        if (divideBy != 0)
            divideBy *= 10;
    }
    if (divideBy != 0)
        *a = *a / divideBy;
    if (negative)
        *a = *a * -1;
    return 1;
}


int getInt(int *a)
{
    char *c = calloc(100, sizeof(int));
    scanf("%s", c);
    if (!strtoint(c, a)) {
        free(c);
        return 0;
    }
    free(c);
    return 1;
}

int getDouble(double *a) {
    char *c = calloc(100, sizeof(int));
    scanf("%s", c);
    if (!strtodouble(c, a)) {
        free(c);
        return 0;
    }
    free(c);
    return 1;
}

int getComplex(complex *a)
{
    printf("Now type real part:\n");
    if (!getDouble(&(a->Re)))
        return 0;
    printf("Now type imaginary part:\n");
    if (!getDouble(&(a->Im)))
        return 0;
    return 1;
}

int getCharPointers(void **a) {
    printf("In the first line type the length of the string, in the second -- the string\n");
    int sz = 0;
    if (!getInt(&sz))
        return 0;
    getchar();
    char *b;
    b = malloc(sz + 1);
    int i = 0;
    for(i; i < sz; i++)
        b[i] = getchar();
    b[sz] = '\0';
    *a = (void*)b;
    return 1;
}

int getVector(vector *created)
{
    printf("...............\nWhat kind of numbers will the vector consist of?\n1 -- integer\n2 -- double (that is real number)\n3 -- complex\n4 -- char* (concatenation only)\n");
    int choice;
    if (!getInt(&choice))
        return 0;
    printf("Now type the value: first X, then Y, then Z\n Note: you should type cartesian orthogonal coordinates (or strings in case you chose so)\n");
    if (choice == 1)
    {
        vector *newV = create(sumInt, multInt, minusInt, sizeof(int));
        memcpy(created, newV, sizeof(vector));
        forget(newV);
        free(newV);
        created->x = malloc(sizeof(int));
        created->y = malloc(sizeof(int));
        created->z = malloc(sizeof(int));
        if (!getInt(created->x) || !getInt(created->y) || !getInt(created->z))
        {
            forget(created);
            return 0;
        }
    }
    else if (choice == 2)
    {
        vector *newV = create(sumDouble, multDouble, minusDouble, sizeof(double));
        memcpy(created, newV, sizeof(vector));
        forget(newV);
        free(newV);
        created->x = malloc(sizeof(double));
        created->y = malloc(sizeof(double));
        created->z = malloc(sizeof(double));
        if (!getDouble(created->x) || !getDouble(created->y) || !getDouble(created->z))
        {
            forget(created);
            return 0;
        }
    }
    else if (choice == 3)
    {
        vector *newV = create(sumComplex, multComplex, minusComplex, sizeof(complex));
        memcpy(created, newV, sizeof(vector));
        forget(newV);
        free(newV);
        created->x = malloc(sizeof(complex));
        created->y = malloc(sizeof(complex));
        created->z = malloc(sizeof(complex));
        if (!getComplex(created->x) || !getComplex(created->y) || !getComplex(created->z))
        {
            forget(created);
            return 0;
        }
    }
    else if (choice == 4)
    {
        vector *newV = create(sumString, 0, 0, 0);
        memcpy(created, newV, sizeof(vector));
        forget(newV);
        free(newV);
        created->x = 0;
        created->y = 0;
        created->z = 0;
        if (!getCharPointers(&((created->x))) || !getCharPointers(&((created->y))) || !getCharPointers(&((created->z))))
        {
            forget(created);
            return 0;
        }
    }
    else
    {
        err5();
        return 0;
    }
    return 1;
}


int pushVectArr(vector **vectArr, int *n)
{
    vector created;
    if (!getVector(&created))
        return 0;
    *vectArr = realloc(*vectArr, (*n + 1) * sizeof(vector));
    (*n)++;
    memcpy(&((*vectArr)[*n - 1]), &created, sizeof(vector));
    return 1;
}

int delVectArr(vector **vectArr, int *n)
{
    if (*n == 0)
    {
        printf("...............\nThere is nothing to delete\n");
        return 0;
    }
    printf("...............\nType the index of vector to be deleted\n");
    int choice;
    if (!getInt(&choice))
        return 0;
    if (choice >= *n || choice < 0)
    {
        err5();
        return 0;
    }
    vector *newArr = calloc(*n - 1, sizeof(vector));
    int i;
    for(i = 0; i < *n; i++)
    {
        if (i != choice)
            newArr[i - (i > choice)] = (*vectArr)[i];
        else
            forget(&((*vectArr)[i]));
    }
    free(*vectArr);
    *n = *n - 1;
    *vectArr = newArr;
    return 1;
}

int push_delVectArr(vector **vectArr, int *n) {
    printf("---------------\nSo what is it we are doing?\n1 -- Creating new vector\n2 -- Deleting an existing vector\n");
    int choice;
    if (!getInt(&choice))
        return 0;
    if (choice > 2 || choice < 1)
    {
        err5();
        return 0;
    }
    if (choice == 1)
    {
        pushVectArr(vectArr, n);
    }
    else
    {
        delVectArr(vectArr, n);
    }
    return 1;
}


int prtVectors(const vector *vectArr, const int n) {
    int i;
    printf("---------------\n%d vector(s) exist\n", n);
    for(i = 0; i < n; i++)
    {
        printf("%2d: ", i);
        if (vectArr[i].sum == sumInt)
        {
            printf("(%d, %d, %d)\n", *((int*)(vectArr[i].x)), *((int*)(vectArr[i].y)), *((int*)(vectArr[i].z)));
        }
        else if (vectArr[i].sum == sumDouble)
        {
            printf("(%.4lf, %.4lf, %.4lf)\n", *((double*)vectArr[i].x), *((double*)(vectArr[i].y)), *((double*)(vectArr[i].z)));
        }
        else if (vectArr[i].sum == sumComplex)
        {
            printf("(%.4lf + %.4lf i, %.4lf + %.4lf i, %.4lf + %.4lf i)\n",
                   (*((complex*)(vectArr[i].x))).Re, (*((complex*)(vectArr[i].x))).Im,
                   (*((complex*)(vectArr[i].y))).Re, (*((complex*)(vectArr[i].y))).Im,
                   (*((complex*)(vectArr[i].z))).Re, (*((complex*)(vectArr[i].z))).Im);
        }
        else
        {
            printf("(%s, %s, %s)\n", (char*)vectArr[i].x, (char*)vectArr[i].y, (char*)vectArr[i].z);
        }
    }
    return 0;
}

int calc(vector **vectArr, const int n) {
    printf("---------------\nWhat kind of a calculation are we running?\n1 -- sum of vectors\n2 -- dot product\n3 -- cross product\n");
    int choice;
    if (!getInt(&choice))
        return 0;
    if (choice > 3 || choice <1)
    {
        err5();
        return 0;
    }
    printf("...............\nType index of the first vector, then of the second one. The result will be put instead of the first vector\n");
    int i1, i2;
    if (!getInt(&i1) || !getInt(&i2))
        return 0;
    if (i1 >= n || i2 >= n || i1 < 0 || i2 < 0)
    {
        err5();
        return 0;
    }
    if (choice == 1)
    {
        vectSum(&((*vectArr)[i1]), &((*vectArr)[i2]));
    }
    else if (choice == 2)
    {
        printf("Your dot product is:\n");
        void *res = dotProduct(&((*vectArr)[i1]), &((*vectArr)[i2]));
        if ((*vectArr)[i1].size == (*vectArr)[i2].size && ((*vectArr)[i1].mult != 0))
        {
            if ((*vectArr)[i1].size == sizeof(int))
                printf("%d\n", *((int *)res));
            else if ((*vectArr)[i1].size == sizeof(double))
                printf("%.4lf\n", *((double*)(res)));
            else
                printf("%.4lf + %.4lf i\n", (*((complex*)res)).Re, (*((complex*)res)).Im);
        }
        free(res);
    }
    else if (choice == 3)
    {
        crossProduct(&((*vectArr)[i1]), &((*vectArr)[i2]));
    }
    printf("Done!\n");
    return 1;
}
