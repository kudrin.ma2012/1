#pragma once

#include <stdio.h>
#include <stdlib.h>
#include "Arithmetics.h"

#ifndef CPROJECT_Vector_H
#define CPROJECT_Vector_H

#endif //CPROJECT_Vector_H

struct vector
{
    void *x, *y, *z;
    void* (*sum)(void*, void*);
    void* (*mult)(void*, void*);
    void* (*minus)(void*, void*);
    int size;
} typedef vector;

///Note: returns pointers
vector* create(void* (*sum)(void*, void*), void* (*mult)(void*, void*), void* (*minus)(void*, void*), int size);
void forget(vector* a);

///Result is memorized in v1
void vectSum(vector* v1, vector* v2);
void *dotProduct(vector* v1, vector *v2);
void crossProduct(vector* v1, vector *v2);