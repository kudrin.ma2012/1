#pragma once
#include <stdio.h>
#include <stdlib.h>

#ifndef CPROJECT_Arithmetics_H
#define CPROJECT_Arithmetics_H

#endif //CPROJECT_Arithmetics_H

struct complex
{
    double Re;
    double Im;

} typedef complex;



void* sumInt(void* a1, void* a2);
void* sumDouble(void* a1, void* a2);
void* sumComplex(void* a1, void* a2);
void* sumString(void *a1, void* a2);

void* minusInt(void *a1, void *a2);
void* minusDouble(void *a1, void *a2);
void* minusComplex(void *a1, void *a2);

void* multInt(void *a1, void *a2);
void* multDouble(void *a1, void *a2);
void* multComplex(void *a1, void *a2);